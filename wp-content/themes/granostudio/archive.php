<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage GranoStudio
 * @since GranoStudio 2.0
 */

get_header('blog'); ?>

	<body class="page-blog">

		<?php
		$args = array( 'post_type' => 'post', 'category_name' => 'destaque','posts_per_page' => 1);
		$loop = new WP_Query( $args );
		?>
		<?php if ( $loop->have_posts() ) : ?>
		<?php while ( $loop->have_posts() ) : $loop->the_post(); $post_id = get_the_ID(); // run the loop ?>

		<section class="section section-post thumbnail2" id="post">

			<!-- <div class="container"> -->
				<!-- <div class="row"> -->
					<div class="">
						<div class="content-text mgb100" style="position: relative;margin-left: -15px;margin-right: -15px;">

								 <!-- thumbnail -->
								 <div class="thumb img-responsive img-post img-full post-animation"
															data-anchor-target="#post"
															data-top-bottom="opacity:1;transform:scale(1) translate3d(0px, -10%, 0px);transform-origin:50% 50%;"
															data-bottom-top="opacity:1;transform:scale(.96) translate3d(0px, 10%, 0px);transform-origin:50% 50%;"
															style="margin-bottom: 30px;margin-top: -16px;margin-left: -2px;">
															<?php the_post_thumbnail(); ?>

															<div class="mask"></div>
															<div class="col-md-6 col-md-offset-3 conteudo-thumbnail">
																<p style="color: #FAFAFA;font-size: 16px;margin-bottom: 10px;">Café com Design</p>
																<h1 class="titulo-single" data-anchor-target="#post"
																data-bottom-top="transform: translate3d(0px, 0%, 0px);"
																data-top-bottom="transform: translate3d(0px, -10%, 0px);"><?php echo get_the_title(); ?></h1>
																<ul class="post-info-top">
																	<!-- <li style="margin-bottom: 15px;"><a onClick="history.go(-1)" style="cursor: pointer;"><span class="arrow-left fa fa-long-arrow-left"></span> Voltar</a></li> -->
																	<li class="" style="text-align: center;padding-top: 10px;line-height: 20px;font-size: 11px;">
																		<span class="date"><?php echo get_the_date(); ?></span> <span class="divider">|</span> <span class="strong category"><?php echo the_category(); ?></span>
																	</li>
																</ul>
																<div class="row" style="margin-top: 30px;">
																	<div class="col-xs-12">
																		<a href="<?php echo get_the_permalink(); ?>" class="btn btn-enviar">Saiba mais</a>
																	</div>
																</div>
															</div>

									</div>


				<!-- </div> -->
				</div>
			<!-- </div> -->
		</section>

	<?php endwhile; // end of the loop. ?>
	<?php endif; ?>


		<section class="conteudo">

			<section class="section section-4 section-blog">
				<div class="container">
					<div class="col-md-6 col-md-offset-3" style="margin-top: 80px;margin-bottom: 20px;">
						<h2 class="txt-big">Últimos Posts</h2>
					</div>
				</div>
			</section>

		<section class="section section-1" id="section-1" style="margin-top: 10px;">
			<div class="container">
				<div class="row content-search">
					<div class="col-xs-12">
							<form role="search" method="get" id="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">
							<div class="form-group">
								<div class="input-group input-group-unstyled">
									<input type="text" class="form-control"	placeholder="Pesquisar..." id="inputGroup" name="s"/>
									<span class="input-group-addon">
										<i class="fa fa-search"></i>
									</span>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</section>
		<div class="clearfix"></div>

		<section class="section section-portfolio" id="portfolio-items" style="background-color: #FAFAFA;">
			<div class="container">
				<div class="row content-center mgt50">

		<?php if ( have_posts() ) : ?>

			<header class="page-header">
				<?php
					the_archive_title( '<h1 class="page-title" style="color: #51403a;">', '</h1>' );
					the_archive_description( '<div class="taxonomy-description">', '</div>' );
				?>
			</header><!-- .page-header -->

			<div class="portfolio-grid blog-02" id="portfolio" data-anchor-target="#portfolio-items" data-top-bottom="top:0;" data-bottom-top="top:-50px;">

			<?php
			// Start the Loop.
			while ( have_posts() ) : the_post();?>


			<div class="col-xs-12 col-sm-4 image-caption item-blog scale-anm design all">
				<a href="<?php echo get_the_permalink(); ?>" class="image">
					<?php if (has_post_thumbnail()): ?>
						<?php the_post_thumbnail( ); ?>
						<div class="caption">
							<span class="anima">
								<span class="icon grano-branco logo-grano">Grano</span>
								<span class="title"><?php echo get_the_title(); ?></span>
								<ul class="lista-categoria">
									<?php
									foreach((get_the_category()) as $category) {
										echo '<li class="hashtags">' . $category->cat_name . '</li>';
									}
									?>
								</ul>
							</span>
						</div>

					<?php else: ?>
						<div class="foto"></div>
						<div class="caption" style="background: none;">
							<span class="anima">
								<span class="icon grano-branco logo-grano">Grano</span>
								<span class="title"><?php echo get_the_title(); ?></span>
								<ul class="lista-categoria">
									<?php
									foreach((get_the_category()) as $category) {
										echo '<li class="hashtags"><' . $category->cat_name . '</li>';
									}
									?>
								</ul>
							</span>
						</div>
					<?php endif; ?>
				</a>
			</div>


			<?php endwhile ;

			// Previous/next page navigation.
			the_posts_pagination( array(
				'prev_text'          => __( 'Previous page', 'twentyfifteen' ),
				'next_text'          => __( 'Next page', 'twentyfifteen' ),
				'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'twentyfifteen' ) . ' </span>',
			) );

		// If no content, include the "No posts found" template.
		else :
			get_template_part( 'content', 'none' );

		endif;
		?>
	</div>

	</div>
	</div>
	</section>

	</section><!-- .content-area -->

<?php get_footer(); ?>
