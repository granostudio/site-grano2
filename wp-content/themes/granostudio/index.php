<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage GranoStudio
 * @since GranoStudio 2.0
 */

get_header(); ?>

<body class="page-home" style="position: absolute; top:0;">

	<section class="section section-1" id="home" style="margin-top: 100px;">
		<div class="container">
			<div class="row home-section-1">
				<div class="col-xs-12 image-fade" data-0="opacity:1;transform:scale(1);transform-origin:50% 50%;" data-500="opacity:0;transform:scale(1.5);transform-origin:50% 50%;">
					<img src="<?php echo MEDIA_PATH; ?>/imgs/home.png" class="img-responsive center img-home">
				</div>

				<div class="col-xs-12 mgb100" data-500="transform:scale(0.5);top:-80%;" data-800="transform:scale(1);top:0%;">
					<h3 class="txt-medium title" data-0="transform:scale(1);" data-500="transform:scale(1.5);" data-650="transform:scale(.5);">Design é aliar o belo ao funcional...</h3>
					<span class="icon arrow-down bounce-arrow"></span>
				</div>
			</div>

			<div class="row mgt100 mgb100 home-section-2">
				<div class="col-xs-12 col-sm-6 image-fade ease-animation" data-0="margin-top:150px;" data-400="margin-top:0;" data-750="margin-top[cubic]:-100px;">
					<img src="<?php echo MEDIA_PATH; ?>/imgs/construindo-identidade.png" class="img-responsive">
				</div>

				<div class="col-xs-12 col-sm-6 text-left ease-animation" data-0="margin-top:250px;" data-400="margin-top:0;" data-750="margin-top[cubic]:-100px;">
					<h2 class="txt-big mgb30 title">É construir uma identidade</h2>

					<div class="pad-left">
						<p class="desc">Do rascunho à concepção da marca, site ou aterial gráfico, o nosso processo consiste em um período de imersão total. Ao pesquisar o mercado, o target e a narrativa do cliente, fazemos emergir as carascterísticas que o tornam único.</p>

						<a href="" class="btn btn-enviar">Conheça nosso processo</a>
					</div>
				</div>
			</div>

			<div class="row mgt100 mgb100">
				<div class="col-xs-12">
					<h3 class="txt-medium ease-animation" data-750="margin-top[cubic]:0px;opacity:.6;" data-900="margin-top[swing]:0;opacity:1;">que comunique sua essência</h3>
					<em><p class="ease-animation" data-750="margin-top[cubic]:80px;opacity:.6;" data-900="margin-top[swing]:0;opacity:1;">em todo e qualquer material...</p></em>
				</div>
			</div>

			<div id="col0-skrollr" class="hidden-xs hidden-sm">
				<div data-anchor-target="#home" class="brush brush-r"
				data-bottom-top="top: 50%;"
				data-top-bottom="top: 0%;"
				></div>
			</div>
		</div>
	</section>
	<div class="clearfix"></div>

	<section class="section section-2" id="o-que-fazemos">
		<div class="container">
			<div class="row ease-animation" data-1050="margin-top[cubic]:80px;opacity:.6;" data-1300="margin-top[swing]:0;opacity:1;">
				<div class="col-xs-12">
					<h2 class="txt-big">nisso nós somos <br>especialistas</h2>
				</div>
			</div>

			<div class="row icons-design">
				<div class="col-xs-5 col-sm-5 col-md-3 item" data-950="transform:scale(0.6);opacity:.6;" data-1100="transform:scale(1);opacity:1;">
					<a href="#" data-toggle="modal" data-target="#modalProduto" style="cursor: context-menu;">
						<img src="<?php echo MEDIA_PATH; ?>/imgs/icon-1.png" class="img-responsive" />
						<span class="title">Design</span>
						<span class="sub-title" style="display: none;">O que é isso</span>
					</a>
				</div>
				<div class="col-xs-6 col-sm-6 col-md-3 item" data-980="transform:scale(0.6);opacity:.6;" data-1250="transform:scale(1);opacity:1;">
					<a href="#" data-toggle="modal" data-target="#modalProduto" style="cursor: context-menu;">
						<img src="<?php echo MEDIA_PATH; ?>/imgs/icon-2.png" class="img-responsive" />
						<span class="title">Naming</span>
						<span class="sub-title" style="display: none;">O que é isso</span>
					</a>
				</div>
				<div class="col-xs-6 col-sm-6 col-md-3 item" data-1010="transform:scale(0.6);opacity:.6;" data-1400="transform:scale(1);opacity:1;">
					<a href="#" data-toggle="modal" data-target="#modalProduto" style="cursor: context-menu;">
						<img src="<?php echo MEDIA_PATH; ?>/imgs/icon-3.png" class="img-responsive" />
						<span class="title">Identidade Visual</span>
						<span class="sub-title" style="display: none;">O que é isso</span>
					</a>
				</div>
				<div class="col-xs-6 col-sm-6 col-md-3 item" data-1040="transform:scale(0.6);opacity:.6;" data-1550="transform:scale(1);opacity:1;">
					<a href="#" data-toggle="modal" data-target="#modalProduto" style="cursor: context-menu;">
						<img src="<?php echo MEDIA_PATH; ?>/imgs/icon-4.png" class="img-responsive" />
						<span class="title">Branding</span>
						<span class="sub-title" style="display: none;">O que é isso</span>
					</a>
				</div>

				<div class="col-xs-6 col-sm-6 col-md-3 item" data-1070="transform:scale(0.6);opacity:.6;" data-1700="transform:scale(1);opacity:1;">
					<a href="#" data-toggle="modal" data-target="#modalProduto" style="cursor: context-menu;">
						<img src="<?php echo MEDIA_PATH; ?>/imgs/icon-5.png" class="img-responsive" />
						<span class="title">Campanhas offline</span>
						<span class="sub-title" style="display: none;">O que é isso</span>
					</a>
				</div>
				<div class="col-xs-6 col-sm-6 col-md-3 item" data-1100="transform:scale(0.6);opacity:.6;" data-1850="transform:scale(1);opacity:1;">
					<a href="#" data-toggle="modal" data-target="#modalProduto" style="cursor: context-menu;">
						<img src="<?php echo MEDIA_PATH; ?>/imgs/icon-6.png" class="img-responsive" />
						<span class="title">Marketing Digital</span>
						<span class="sub-title" style="display: none;">O que é isso</span>
					</a>
				</div>
				<div class="col-xs-6 col-sm-6 col-md-3 item" data-1130="transform:scale(0.6);opacity:.6;" data-2000="transform:scale(1);opacity:1;">
					<a href="#" data-toggle="modal" data-target="#modalProduto" style="cursor: context-menu;">
						<img src="<?php echo MEDIA_PATH; ?>/imgs/icon-7.png" class="img-responsive" />
						<span class="title">Websites & e-commerce</span>
						<span class="sub-title" style="display: none;">O que é isso</span>
					</a>
				</div>
			</div>
		</div>
	</section>
	<div class="clearfix"></div>

	<section class="section section-3" id="o-que-fizemos">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 ease-animation" data-0="bottom:-250px;opacity:.8;" data-1300="bottom:-150px;opacity:1;" data-1800="bottom:0;">
					<h2 class="txt-big">O que fizemos</h2>
					<p><em>mais do que clientes, orgulho em chama-los de parceiros</em></p>
				</div>
			</div>

			<div class="row grid-portfolio">

			<?php
			 $args = array( 'post_type' => 'projeto', 'posts_per_page' => 6);
			 $loop = new WP_Query( $args );

			 if ( $loop->have_posts() ) : while ( $loop->have_posts() ) : $loop->the_post(); $post_id = get_the_ID(); // run the loop ?>

			 <div class="col-xs-12 col-sm-6 col-md-4 item ease-animation" data-anchor-target="#o-que-fizemos" data-top-bottom="top:0px;margin-top:5px;" data-bottom-top="top:100px;margin-top:30px;">
				 <a href="<?php echo get_the_permalink(); ?>" class="image">
					 <img src="<?php the_post_thumbnail(); ?>" >
				 </a>
			 </div>


			 <?php endwhile; // end of the loop. ?>
			 <?php endif; ?>

			</div>
		</div>
	</section>
	<div class="clearfix"></div>

	<section class="section section-4 section-blog" id="blog">
		<div id="col1-skrollr" class="hidden-xs hidden-sm">
			<div data-anchor-target="#home" class="brush brush-l"
			data-bottom-top="top: 50%;"
			data-top-bottom="top: 0%;"
			></div>
		</div>

		<div class="container">
			<!-- <div class="row content-center ease-animation" data-anchor-target="#blog" data-bottom-top="top:50px;margin-top:50px;" data-top-bottom="top:-150px;margin-top:0;">
				<div class="col-xs-12 col-sm-5 text-left vcenter col">
					<h2 class="txt-medium">Bem vindo ao <br> Café com Design</h2>
					<p>Tendências no mercado, discussões sobre Design, Usabilidade, Arte, Publicidade e muito mais</p>
				</div>
				<div class="col-xs-12 col-sm-7 vcenter col image-caption item-blog-big">
					<?php
					$args = array( 'post_type' => 'post', 'category_name' => 'destaque', 'posts_per_page' => 1);
					$loop = new WP_Query( $args );
					?>
					<?php if ( $loop->have_posts() ) : ?>
					<?php while ( $loop->have_posts() ) : $loop->the_post(); $post_id = get_the_ID(); // run the loop ?>

					<a href="<?php echo get_the_permalink(); ?>" class="image"> -->
						<!-- <img src="http://via.placeholder.com/350x350" class="rz"> -->
						<!-- <?php the_post_thumbnail(); ?>
						<div class="caption">
							<span class="anima">
								<span class="icon grano-branco logo-grano">Grano</span>
								<span class="title"><?php echo get_the_title(); ?></span>
								<ul class="lista-categoria-index">
									<?php
										foreach((get_the_category()) as $category) {
												echo '<li class="hashtags">' . $category->cat_name . '</li>';
										}
										?>
								</ul>
							</span>
						</div>
					</a>
				<?php endwhile; // end of the loop. ?>
				<?php endif; ?>

				</div>
			</div> -->

			<div class="row">
				<div class="col-xs-12 ease-animation" data-0="bottom:-250px;opacity:.8;" data-1300="bottom:-150px;opacity:1;" data-1800="bottom:0;" style="margin-bottom: 20px;margin-top: 50px;">
					<h2 class="txt-big">Conheça nosso Blog</h2>
				</div>
			</div>

			<section class="section section-1" id="section-1" style="background-color: #FAFAFA;">
					<div class="container">
						<div class="row content-search content-search2">
							<div class="col-xs-12">
									<form role="search" method="get" id="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">
									<div class="form-group">
										<div class="input-group input-group-unstyled">
											<input type="text" class="form-control"	placeholder="Pesquisar..." id="inputGroup" name="s"/>
											<span class="input-group-addon">
												<i class="fa fa-search"></i>
											</span>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>

			</section>

			<div class="row portfolio-grid section-blog" data-anchor-target="#blog" data-bottom-top="top:50px;margin-top:0px;" data-top-bottom="top:-120px;margin-top:0;">

				<?php

			 $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
			 $args = array( 'post_type' => 'post', 'posts_per_page' => 6, 'paged' => $paged, 'page' => $paged);
			 $loop = new WP_Query( $args );

			 if ( $loop->have_posts() ) : while ( $loop->have_posts() ) : $loop->the_post(); $post_id = get_the_ID(); // run the loop ?>

				<div class="col-xs-12 col-sm-4 image-caption item-blog scale-anm design all">
					<a href="<?php echo get_the_permalink(); ?>" class="image">
						<?php if (has_post_thumbnail()): ?>
              <?php the_post_thumbnail( ); ?>
							<div class="caption">
								<span class="anima">
									<span class="icon grano-branco logo-grano">Grano</span>
									<span class="title"><?php echo get_the_title(); ?></span>
									<ul class="lista-categoria">
										<?php
										foreach((get_the_category()) as $category) {
											echo '<li class="hashtags">' . $category->cat_name . '</li>';
										}
										?>
									</ul>
								</span>
							</div>

            <?php else: ?>
              <div class="foto"></div>
							<div class="caption no-mask">
								<span class="anima">
									<span class="icon grano-branco logo-grano">Grano</span>
									<span class="title"><?php echo get_the_title(); ?></span>
									<ul class="lista-categoria">
										<?php
										foreach((get_the_category()) as $category) {
											echo '<li class="hashtags">' . $category->cat_name . '</li>';
										}
										?>
									</ul>
								</span>
							</div>
            <?php endif; ?>
					</a>
				</div>

			<?php endwhile; // end of the loop. ?>
			<?php endif; ?>

			</div>

			<div class="row">
				<div class="col-xs-12">
					<a href="site-grano-william/blog" class="btn btn-enviar">Ir para o Blog</a>
				</div>
			</div>
		</div>
	</section>
	<div class="clearfix"></div>

	<section class="section section-5" id="contato">
		<div class="container">
			<div class="row mgt30 mgb50">
				<div class="col-xs-12 ease-animation" data-anchor-target="#contato" data-bottom-top="top:80px;margin-top:50px;" data-top-bottom="top:0;margin-top:0;">
					<img src="<?php echo MEDIA_PATH; ?>/imgs/coffee.png" class="mgb15" />
					<h2 class="txt-medium">Vamos tomar um café?</h2>
					<p>Entre em contato e agende uma reunião, o café é por nossa conta ;)</p>
				</div>
			</div>

			<div class="row">
				<div class="col-xs-12 col-sm-10 col-sm-offset-1 formularios mgt30">
					<form>
						<!-- <div class="form-group">
							<input type="text" class="form-control" placeholder="Nome">
						</div>

						<div class="form-group">
							<input type="text" class="form-control" placeholder="Telefone">
						</div>

						<div class="form-group">
							<input type="email" class="form-control" placeholder="Email">
						</div>

						<div class="form-group">
							<textarea class="form-control noresize" rows="5" placeholder="Mensagem"></textarea>
						</div> -->

						<?php echo do_shortcode ('[contact-form-7 id="12" title="Formulário de contato 1"]') ?>

						<!-- <button type="submit" class="btn btn-enviar">Enviar</button> -->
					</form>
				</div>
			</div>
		</div>
	</section>

	<!-- <div id="modalProduto" class="modal fade" tabindex="-1" role="dialog">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">Produto</h4>
				</div>
				<div class="modal-body">
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
					tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
					quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
					consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
					cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
					proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
				</div>
			</div>
		</div>
	</div> -->

	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDYo9dDIVSiOyYpppqDb5AwlcZg0d0esyM"></script>
	<script src="<?php echo MEDIA_PATH; ?>/js/scripts.js"></script>

<?php get_footer(); ?>
