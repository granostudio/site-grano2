<?php
/**
 * GranoStudio functions and definitions
 *
 * Set up the theme and provides some helper functions, which are used in the
 * theme as custom template tags. Others are attached to action and filter
 * hooks in WordPress to change core functionality.
 *
 * When using a child theme you can override certain functions (those wrapped
 * in a function_exists() call) by defining them first in your child theme's
 * functions.php file. The child theme's functions.php file is included before
 * the parent theme's file, so the child theme functions would be used.
 *
 * @link https://codex.wordpress.org/Theme_Development
 * @link https://codex.wordpress.org/Child_Themes
 *
 * Functions that are not pluggable (not wrapped in function_exists()) are
 * instead attached to a filter or action hook.
 *
 * For more information on hooks, actions, and filters,
 * {@link https://codex.wordpress.org/Plugin_API}
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

/**
 * Set the content width based on the theme's design and stylesheet.
 *
 * @since GranoStudio 2.0
 */

function wpt_theme_styles() {

  wp_enqueue_style( 'single_css', get_template_directory_uri() . '/css/single.css' );

  wp_enqueue_style( 'main_css', get_template_directory_uri() . '/style.css' );

}
add_action('wp_enqueue_scripts','wpt_theme_styles');


add_theme_support( 'menus');

// Esse tema usa-se wp_nav_menu() em dois locais.
function register_theme_menus() {

  register_nav_menus(
    array(
      'primary-menu' => __( 'Menu Principal' ),
      'second-menu' => __( 'Menu Secundario' ),
    )
  );

}
add_action( 'init', 'register_theme_menus' );

// adiciona a thumbnail
add_theme_support( 'post-thumbnails' );
set_post_thumbnail_size( 1200, 9999 );


// customização da página wp-admin
function my_login_logo() { ?>
    <style type="text/css">
        #login h1 a, .login h1 a {
            background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/media/imgs/logo.png);
        		background-repeat: no-repeat;
        }
        input .wp-core-ui .button-primary {
          background: #50403a;
        }
    </style>
<?php }
add_action( 'login_enqueue_scripts', 'my_login_logo' );


// Um custom post type chamado 'projeto'
	function wptutsplus_create_post_type() {
	  $labels = array(
	        'name' => __( 'Projetos' ),
	        'singular_name' => __( 'projeto' ),
	        'add_new' => __( 'Novo projeto' ),
	        'add_new_item' => __( 'Adicionar Novo projeto' ),
	        'edit_item' => __( 'Editar projeto' ),
	        'new_item' => __( 'Novo projeto' ),
	        'view_item' => __( 'View banner' ),
	        'search_items' => __( 'Search banners' ),
	        'not_found' =>  __( 'Projetos não encontrados' ),
	        'not_found_in_trash' => __( 'No banners found in Trash' ),
	    );
	    $args = array(
	        'labels' => $labels,
	        'has_archive' => true,
	        'public' => true,
	        'hierarchical' => false,
	        'supports' => array(
	            'title',
	            'editor',
	            'excerpt' => false,
	            'custom-fields' => false,
	            'thumbnail',
	            'page-attributes' => false,
	        ),
	        'taxonomies' => array( 'post_tag', 'category'),
	    );
	    register_post_type( 'projeto', $args );
	}
	add_action( 'init', 'wptutsplus_create_post_type' );


add_action( 'cmb2_admin_init', 'cmb2_sample_metaboxes' );


//Configurações e criação de metabox.
function cmb2_sample_metaboxes() {

    // Start with an underscore to hide fields from custom fields list
    $prefix = '_yourprefix_';

    /**
     * Initiate the metabox
     */
    $cmb = new_cmb2_box( array(
        'id'            => 'test_metabox',
        'title'         => __( 'Banner', 'cmb2' ),
        'object_types'  => array( 'projeto', ), // Post type
        'context'       => 'normal',
        'priority'      => 'high',
        'show_names'    => true, // Show field names on the left
        // 'cmb_styles' => false, // false to disable the CMB stylesheet
        // 'closed'     => true, // Keep the metabox closed by default
    ) );

    $cmb->add_field( array(
        'name' => 'Lista de imagens',
        'desc' => 'Adicionar as imagens',
        'id'   => 'wiki_test_file_list',
        'type' => 'file_list',
        'preview_size' => array( 100, 100 ), // Default: array( 50, 50 )
            // 'query_args' => array( 'type' => 'image' ), // Only images attachment
        // Optional, override default text strings
        'text' => array(
            'add_upload_files_text' => 'Adicionar', // default: "Add or Upload Files"
            'remove_image_text' => 'Remover', // default: "Remove Image"
            'file_text' => 'Replacement', // default: "File:"
            'file_download_text' => 'Replacement', // default: "Download"
            'remove_text' => 'Replacement', // default: "Remove"
        ),
    ) );

  }

add_action('init', 'my_category_module');

function my_category_module() {
  add_action ( 'edit_category_form_fields', 'add_image_cat');
  add_action ( 'edited_category', 'save_image');
}

function add_taxonomies_to_pages() {
 register_taxonomy_for_object_type( 'post_tag', 'page' );
 register_taxonomy_for_object_type( 'category', 'page' );
 }
add_action( 'init', 'add_taxonomies_to_pages' );
