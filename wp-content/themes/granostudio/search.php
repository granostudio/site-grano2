<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage GranoStudio
 * @since GranoStudio 2.0
 */

get_header(); ?>

<section class="section section-1" id="section-1" style="margin-top: 100px;">
    <div class="container">
      <div class="row content-search">
        <div class="col-xs-12">
            <form role="search" method="get" id="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">
            <div class="form-group">
              <div class="input-group input-group-unstyled">
                <input type="text" class="form-control"	placeholder="Pesquisar..." id="inputGroup" name="s"/>
                <span class="input-group-addon">
                  <i class="fa fa-search"></i>
                </span>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </section>

<div class="portfolio-grid blog-02" id="portfolio" data-anchor-target="#portfolio-items" data-top-bottom="top:0;" data-bottom-top="top:-50px;">

<div class="container">

  <header class="page-header">
    <h1 class="page-title"><?php printf( __( 'Resultado de: %s', 'shape' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
    </header><!-- .page-header -->

    <?php
        $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
        $args = array( 'post_type' => 'post', 'posts_per_page' => 9, 'paged' => $paged, 'page' => $paged);
    ?>
    <?php
    if( have_posts() ) {
      while ( have_posts() ) {
        the_post();?>

        <div class="col-xs-12 col-sm-4 image-caption item-blog scale-anm design all">
          <a href="<?php echo get_the_permalink(); ?>" class="image">
            <?php the_post_thumbnail(); ?>
            <div class="caption">
              <span class="anima">
                <span class="icon grano-branco logo-grano">Grano</span>
                <span class="title"><?php echo get_the_title(); ?></span>
                <span class="hashtags">#lorem #lorem</span>
              </span>
            </div>
          </a>
        </div>


    <?php
      }
    }
     else {
      echo "nada encontrado";
    }
     ?>

   </div>
</div>

<?php get_footer(); ?>
