<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage GranoStudio
 * @since GranoStudio 2.0
 */

get_header(); ?>

<body class="page-project-v2 mgb50" style="margin-top: -32px;">



<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

	<section class="page-section section section-1" id="section-1" style="margin-top: 100px;">
		<div class="slides project-animation" style="width: 100%;" data-bottom-top="opacity:.8;transform-origin:50% 50%;margin-top:20px;" data-top-bottom="opacity:1;transform-origin:50% 50%;">
			<div id="carousel-project" class="carousel" style="width: 100%;" data-0="opacity:1;transform:scale(1);transform-origin:50% 50%;" data-500="opacity:0.7;transform:scale(1.5);transform-origin:50% 50%;">

				<?php $files = get_post_meta( get_the_ID(), 'wiki_test_file_list', 1 );

				// Loop para banner
				foreach ( (array) $files as $attachment_id => $attachment_url ) {
						echo '<div class="item" data-caption="">';
						echo wp_get_attachment_image( $attachment_id, $img_size );
						echo '</div>';
				}
				 ?>

			</div>
			<div class="clearfix"></div>
			<ul id="nav" class="nav-dots"></ul>
	        <a href="#" class="nav-page page-left"><span id="project-prev" class="fa fa-angle-left"></span></a>
	        <a href="#" class="nav-page page-right"><span id="project-next" class="fa fa-angle-right"></span></a>

		</div>
	</section>
	<div class="clearfix"></div>


	<section class="section section-text mgt50">
		<div class="container">

			<div class="col-xs-12 col-sm-8 text-left col-left">
				<h2 class="txt-medium">Cliente: <?php echo get_the_title(); ?></h2>
				<p class="txt-green mgt30 mgb30">Serviços desenvolvidos</p>

				<ul class="list-inline icons-services mgb30">

					<?php foreach((get_the_category()) as $category) {
						echo '
						<li data-bottom-top="transform:scale(0.7);opacity:.8;" data-top-bottom="transform:scale(1);opacity:1;">
								<img src="'.'/site-grano-william/wp-content/uploads/2017/10/'.$category->category_nicename.'.png" />

						</li>
						';
					}?>

				</ul>

				<p><?php the_content(); ?></p>

			</div>

			<?php endwhile; // end of the loop. ?>

			<div class="col-xs-12 col-sm-4 text-left col-right">
				<h2 class="txt-medium">Cases em destaque</h2>
				<div class="clearfix"></div>

				<div class="grid-portfolio ease-animation" data-0="opacity:.6;" data-400="opacity:1;">
					<div class="row">

						<?php
						 $args = array( 'post_type' => 'projeto', 'posts_per_page' => 2);
						 $loop = new WP_Query( $args );

						 if ( $loop->have_posts() ) : while ( $loop->have_posts() ) : $loop->the_post(); $post_id = get_the_ID(); // run the loop ?>

						 <div class="col-xs-12 item ease-animation">
							 <a href="<?php echo get_the_permalink(); ?>" class="image">
 		 					 <img src="<?php the_post_thumbnail(); ?>" >
 		 				 </a>
 						</div>

					 <?php endwhile; // end of the loop. ?>
					 <?php endif; ?>

					</div>
				</div>
			</div>
		</div>

		<div class="clearfix"></div>

		<div class="back-page container mgt30">
			<div class="col-xs-12">
				<a onClick="history.go(-1)" style="cursor: pointer;"><span class="arrow-left fa fa-long-arrow-left"></span> Voltar</a>
			</div>
		</div>
	</section>
	<div class="clearfix"></div>

	<script src="<?php echo MEDIA_PATH; ?>/js/scripts.js"></script>

<?php get_footer(); ?>
